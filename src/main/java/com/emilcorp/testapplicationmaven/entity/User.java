package com.emilcorp.testapplicationmaven.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;

@Entity(name = "[User]")
public class User extends BaseEntity{
    private String firstName;
    private String lastName;
    private String address;
    private String city;

    @Column(name = "FirstName")
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    @Column(name = "LastName")
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    @Column(name = "Address")
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
    @Column(name = "City")
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
