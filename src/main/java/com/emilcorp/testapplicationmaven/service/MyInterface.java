package com.emilcorp.testapplicationmaven.service;

public interface MyInterface {
    default String test() {
        return "TestFromMyInterface";
    }
}
