package com.emilcorp.testapplicationmaven.service;

public interface MyInterfaceTwo {
    default String test() {
        return "TestFromMyInterfaceTwo";
    }
}
