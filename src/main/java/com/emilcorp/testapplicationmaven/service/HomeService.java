package com.emilcorp.testapplicationmaven.service;

import com.emilcorp.testapplicationmaven.annotation.Loggable;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Service;

@Service
public class HomeService implements HomeServiceInterface{
    @Loggable
    public String testLoggable () {
        return "test";
    }
}
