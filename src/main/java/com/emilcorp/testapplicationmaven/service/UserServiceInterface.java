package com.emilcorp.testapplicationmaven.service;

import com.emilcorp.testapplicationmaven.entity.User;

import java.util.List;
import java.util.Optional;

public interface UserServiceInterface {
    Optional<User> getUserDetails(Long id);
    List<User> getAll();
    User createUser(User user);
}
