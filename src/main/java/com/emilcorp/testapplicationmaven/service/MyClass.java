package com.emilcorp.testapplicationmaven.service;

import java.util.HashMap;
import java.util.Map;

public class MyClass{
    public void map() {
        Map<String, String> nameToAge = new HashMap<>();
        nameToAge.put("Emil", "27");
        nameToAge.put("Ivana", "22");
    }
}
