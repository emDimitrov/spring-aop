package com.emilcorp.testapplicationmaven.service;

import com.emilcorp.testapplicationmaven.entity.User;
import com.emilcorp.testapplicationmaven.repository.UserRepository;
import com.zaxxer.hikari.util.IsolationLevel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class UserService implements UserServiceInterface{

    private UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Transactional(isolation = Isolation.READ_UNCOMMITTED)
    public Optional<User> getUserDetails(Long id) {
        return userRepository.findById(id);
    }

    @Override
    @Transactional(isolation = Isolation.READ_UNCOMMITTED)
    public List<User> getAll() {
        return userRepository.findAll();
    }

    public User createUser(User user) {
        return userRepository.save(user);
    }
}
