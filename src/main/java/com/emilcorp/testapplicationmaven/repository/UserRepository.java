package com.emilcorp.testapplicationmaven.repository;

import com.emilcorp.testapplicationmaven.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
}
