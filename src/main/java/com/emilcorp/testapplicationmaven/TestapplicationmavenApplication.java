package com.emilcorp.testapplicationmaven;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestapplicationmavenApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestapplicationmavenApplication.class, args);
	}

}
