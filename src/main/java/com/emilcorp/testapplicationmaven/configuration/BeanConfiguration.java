package com.emilcorp.testapplicationmaven.configuration;

import com.emilcorp.testapplicationmaven.aspect.LoggableAspect;
import org.springframework.aop.aspectj.annotation.AnnotationAwareAspectJAutoProxyCreator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanConfiguration {
    @Bean
    public LoggableAspect getLoggableAspect() {
        return new LoggableAspect();
    }

    @Bean
    public AnnotationAwareAspectJAutoProxyCreator getAnnotationAwareAspectJAutoProxyCreator() {
        return new AnnotationAwareAspectJAutoProxyCreator();
    }
}
