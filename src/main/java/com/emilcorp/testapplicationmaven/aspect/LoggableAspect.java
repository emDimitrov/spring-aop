package com.emilcorp.testapplicationmaven.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;

import java.util.logging.Level;
import java.util.logging.Logger;

@Aspect
public class LoggableAspect {

    @Pointcut("@annotation(com.emilcorp.testapplicationmaven.annotation.Loggable)")
    public void aspectPointCutDefinition() {
    }

    @Before("aspectPointCutDefinition()")
    public void executeBeforeLoggable(JoinPoint joinPoint) {
        System.out.println(String.format(
                "Entering Method {%s} of {%s}",
                joinPoint.getSignature().getName(),
                joinPoint.getSignature().getDeclaringType()));
    }

    @After("aspectPointCutDefinition()")
    public void executeAfterLoggable(JoinPoint joinPoint) {
        System.out.println(String.format(
                "Exiting Method {%s} of {%s}",
                joinPoint.getSignature().getName(),
                joinPoint.getSignature().getDeclaringType()));
    }

    @Around("aspectPointCutDefinition()")
    public Object executeAroundLoggable(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        long before = System.currentTimeMillis();
        Object proceed = proceedingJoinPoint.proceed();
        long after = System.currentTimeMillis();
        System.out.println("Execution time " + (after - before) + " ms");
        return proceed;
    }


    @AfterThrowing(value = "aspectPointCutDefinition()", throwing = "ex")
    public void executeAfterThrowingLoggable(RuntimeException ex) {
        System.out.println(ex.getMessage());
    }

    @AfterReturning(value = "aspectPointCutDefinition()", returning = "returnObject")
    public void executeAfterReturnLoggable(Object returnObject) {
        System.out.println("Return value {" + returnObject + "}");
    }
}
