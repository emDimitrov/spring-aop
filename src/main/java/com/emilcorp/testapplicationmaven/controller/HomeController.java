package com.emilcorp.testapplicationmaven.controller;

import com.emilcorp.testapplicationmaven.annotation.Loggable;
import com.emilcorp.testapplicationmaven.service.HomeService;
import com.emilcorp.testapplicationmaven.service.HomeServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashSet;
import java.util.Set;

@RestController
public class HomeController {

    private HomeServiceInterface homeService;

    @Autowired
    public HomeController(HomeServiceInterface homeService) {
        this.homeService = homeService;
    }
    //@Loggable
    @GetMapping(path = "/test")
    public String testEndpoint() {
        Set<String> test = new HashSet<>();
        test.add("test");
        test.add("test2");

        System.out.println("Test endpoint was called");
        homeService.testLoggable();
        return "test";
    }
}
