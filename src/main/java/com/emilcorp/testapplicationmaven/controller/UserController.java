package com.emilcorp.testapplicationmaven.controller;

import com.emilcorp.testapplicationmaven.dto.UserDTO;
import com.emilcorp.testapplicationmaven.entity.User;
import com.emilcorp.testapplicationmaven.service.UserService;
import com.emilcorp.testapplicationmaven.service.UserServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/user")
public class UserController {
    private UserServiceInterface userService;

    @Autowired
    public UserController(UserServiceInterface userService) {
        this.userService = userService;
    }

    @GetMapping(path = "/{id}")
    public ResponseEntity<User> get(@PathVariable(name = "id") Long userId) {
        Optional<User> user = userService.getUserDetails(userId);
        if(user.isPresent()) {
            return ResponseEntity.ok(user.get());
        }

        throw new RuntimeException("User with id " + user + " was not found");
    }

    @GetMapping
    public ResponseEntity<List<User>> getAll() {
        List<User> users = userService.getAll();

        return ResponseEntity.ok(users);
    }

    @PostMapping
    public ResponseEntity<User> create(@RequestBody UserDTO userDTO) {
        User user = new User();
        user.setAddress(userDTO.getAddress());
        user.setCity(userDTO.getCity());
        user.setFirstName(userDTO.getFirstName());
        user.setLastName(userDTO.getLastName());

        User userEntity = userService.createUser(user);

        return ResponseEntity.ok(userEntity);
    }
}
